#!/bin/bash


# Install required packages
pacman --noconfirm -S \
    texlive-core \
    texinfo \
    pdf2svg \
    librsvg \
    pandoc \
    python-pandocfilters \
    m4 \
    zip \
    unzip \
    wget \
    bc \
    inkscape \
    nodejs \
    npm \
    libcurl-gnutls \
    patch \
    binutils

# Install texlive packages
patch /usr/share/texmf-dist/scripts/texlive/tlmgr.pl -i /tlmgr.patch
ln -s /usr/share/texmf-dist/scripts/texlive/tlmgr.pl /usr/bin/tlmgr
tlmgr init-usertree
tlmgr option repository https://ctan.math.illinois.edu/systems/texlive/tlnet/
tlmgr install stmaryrd enumitem cleveref braket xypic

# Install html-xml-utils from debian since
# It's not in arch repo
mkdir xmlutils && cd ./xmlutils
curl -OL "http://http.us.debian.org/debian/pool/main/h/html-xml-utils/html-xml-utils_7.7-1_amd64.deb"
echo -n "6f6be748054499654708f57da0270c9c html-xml-utils_7.7-1_amd64.deb" | md5sum -c || exit
ar x html-xml-utils_7.7-1_amd64.deb
tar xvJf data.tar.xz
mv ./usr/bin/hxnum /usr/bin
mv ./usr/bin/hxmultitoc /usr/bin
cd .. && rm -rf xmlutils

# Install nodejs packages
npm init --yes
npm install -g html-minifier
npm install -g svgo
npm install -g stylelint
npm install -g stylelint-config-standard
npm install -g cssnano
npm install -g postcss postcss-cli

# Installing calibre from the repository also installs a lot of
# unwanted dependencies. We only need the ebook-convert
mkdir calibre && cd calibre
curl "https://github.com/kovidgoyal/calibre/releases/latest" > rawlink
cut -d'"' -f2 rawlink | sed -E "s|$|/calibre-"$(cut -d"/" -f8 rawlink | cut -d'"' -f1 | tr -d "v")"-x86_64.txz|" | sed -E "s|tag|download|g" > link
xargs -n1 curl -OL < link
tar xf $(ls -1 | grep -E ".*-x86_64\.txz")
rm link rawlink $(ls -1 | grep -E ".*-x86_64\.txz")
cd .. && mv calibre /opt

# Clean the cache
yes | LC_ALL=en_US.UTF-8 pacman -Scc
sync
