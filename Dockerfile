FROM archlinux:latest
RUN pacman-key --init
RUN pacman --noconfirm -Syu
ADD tlmgr.patch /
ADD install_dependencies.sh /
RUN /install_dependencies.sh && rm -rf /install_dependencies.sh /tlmgr.patch
